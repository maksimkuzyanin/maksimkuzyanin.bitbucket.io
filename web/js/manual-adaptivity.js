var MIN_IMG_WIDTH = 20;
var MAX_IMG_WIDTH = 90;
var MAX_MARGIN_TOP = 156;
var SCREEN_WIDTH = 1519;

$(document).ready(function () {
	// первоначальная инициализация
	init();

	$(window).resize(function() {
		responsiveNavbar();
		responsiveLogoHeader();
		tweakMarginTopByLogoHeader();
	});
});

// инициализирует положения элементов, размеры и т. д.
function init() {
	responsiveNavbar();
	responsiveLogoHeader();
	tweakMarginTopByLogoHeader();
};

// устанавливает padding для navbar
function responsiveNavbar() {
	var hamburgerVisible = $(".navbar-toggler-icon").is(":visible");

	if (!hamburgerVisible) {
		var cardsWidth = $(".cards").width();
		$(".menu .navbar").css("padding-left", cardsWidth);
	}
	else {
		$(".menu .navbar").css("padding-left", 0);
	}
};

// изменяет размер логотипа в шапке
function responsiveLogoHeader() {
	// div.inner-header-container
	var container = $(".inner-header-container");
	// div.logo-header
	var logoHeader = $(".logo-header");

	var $window = $(window);
	var coeff = $window.width() / SCREEN_WIDTH;

	if (coeff > 1) {
		coeff = 1;
	}

	var zoomWidth = Math.round(MAX_IMG_WIDTH * coeff);

	if (zoomWidth > MAX_IMG_WIDTH) {
		zoomWidth = MAX_IMG_WIDTH;
	}
	else if (zoomWidth < MIN_IMG_WIDTH) {
		zoomWidth = MIN_IMG_WIDTH;
	}

	var zoomMargin = container.height() - zoomWidth - 1;

	logoHeader.css("margin-top", zoomMargin);
	logoHeader.find("img").css("width", zoomWidth);
};

function tweakMarginTopByLogoHeader() {
	// div.inner-header-container
	var container = $(".inner-header-container");
	// div.logo-header
	var logoHeader = $(".logo-header");

	var marginTop = container.height() - logoHeader.height() - 1;
	if (marginTop > MAX_MARGIN_TOP) {
		marginTop = MAX_MARGIN_TOP;
	}

	logoHeader.css("margin-top", marginTop);
};
